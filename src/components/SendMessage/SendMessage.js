import React from "react";
import "./SendMessage.css";
import constants from "../../helpers/constants";

class SendMessage extends React.Component {
  render = () => (
    <div className="chat__edit">
      <div className="chat__text">
        <textarea
          name="send-message"
          id="send-message"
          cols="5"
          rows="3"
          className="chat__input"
        />
        <button
          type="button"
          className="chat__send"
          onClick={this.handleSendClick}
        >
          <i className="far fa-paper-plane" />
          Send
        </button>
      </div>
    </div>
  );

  handleSendClick = () => {
    const messageInput = document.getElementById("send-message");
    const newMessage = {
      id: Date.now().toString(),
      user: constants.USER_NAME,
      avatar: constants.USER_AVATAR,
      created_at: (new Date()).toGMTString("en-us"), // store message date in UTC tiemzone 
      message: messageInput.value,
      likes: 0,
      ownedByMe: true,
      likedByMe: false
    };
    this.props.addMessage(newMessage);
    messageInput.value = "";
  };
}

export default SendMessage;
