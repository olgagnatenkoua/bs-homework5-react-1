import React from "react";

import { messageService } from "../../services/messageService";
import Spinner from "../Spinner/Spinner";
import Header from "../Header/Header";
import MessageList from "../MessageList/MessageList";
import SendMessage from "../SendMessage/SendMessage";

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      header: {
        participants: 0,
        messages: 0,
        lastMessageAt: null
      },
      loading: false,
      launchedAt: Date.now().toString(),
      editMode: false
    };
  }

  componentDidMount() {
    this.setState({
      ...this.state,
      loading: true
    });

    try {
      messageService.getInitialMessages().then(messages => {
        let chatMessages = this.enrichMessageData(messages);
        chatMessages = chatMessages.sort(
          (msgA, msgB) => new Date(msgA.created_at) - new Date(msgB.created_at)
        );
        const header = this.makeHeaderInfo(chatMessages);
        this.setState({
          ...this.state,
          messages: chatMessages,
          loading: false,
          header
        });
      });
    } catch (error) {
      console.log(error);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    // only change scroll if message count changes
    const { header } = prevState;
    if (this.state.header.messages !== header.messages) {
      this.scrollToBottom();
    }
  }

  render() {
    const { loading } = this.state;
    return (
      <div className="chat__container">
        {loading ? <Spinner /> : this.renderChat(this.state)}
      </div>
    );
  }

  renderChat = ({ messages, header, launchedAt }) => {
    const handlers = {
      likeMessage: this.likeMessage,
      addMessage: this.addMessage,
      deleteMessage: this.deleteMessage
    };
    return (
      <div className="chat">
        <Header header={header} />
        <MessageList
          messages={messages}
          launchedAt={launchedAt}
          handlers={handlers}
        />
        <SendMessage addMessage={this.addMessage} />
      </div>
    );
  };

  likeMessage = id => {
    const { messages } = this.state;
    const updatedMessages = [...messages];
    const likedMessageIdx = updatedMessages.findIndex(msg => msg.id === id);
    if (likedMessageIdx === -1) {
      return;
    }
    const likedMessage = updatedMessages[likedMessageIdx];
    likedMessage.likedByMe = !likedMessage.likedByMe;
    likedMessage.likes = likedMessage.likes + (likedMessage.likedByMe ? 1 : -1);
    this.setState({
      ...this.state,
      messages: updatedMessages
    });
  };

  addMessage = message => {
    const updatedMessages = [...this.state.messages, message];
    this.setState({
      ...this.state,
      messages: updatedMessages,
      header: {
        ...this.state.header,
        messages: this.state.header.messages + 1,
        lastMessage: new Date(message.created_at + " GMT+0000")
      }
    });
  };

  deleteMessage = id => {
    const { messages } = this.state;
    const deletedMessageIdx = messages.findIndex(msg => msg.id === id);
    if (deletedMessageIdx === -1) {
      return;
    }
    const updatedMessages = [...messages];
    updatedMessages.splice(deletedMessageIdx, 1);

    this.setState({
      ...this.state,
      messages: updatedMessages,
      header: {
        ...this.state.header,
        messages: this.state.header.messages - 1
      }
    });
  };

  editMessage = id => {
    this.setState({
      ...this.state,
      editMode: true
    });
  };

  scrollToBottom = () => {
    const chatElement = document.querySelector(".chat__body");
    if (chatElement) {
      chatElement.scrollTo(0, chatElement.scrollHeight);
    }
  };

  makeHeaderInfo = messages => {
    const participants = new Set(messages.map(msg => msg.user));
    let lastMessage = "Unknown";
    const lastMessageIdx = messages.length - 1;
    if (messages && messages[lastMessageIdx]) {
      lastMessage = new Date(messages[lastMessageIdx].created_at + " GMT+0000");
    }

    return {
      participants: participants.size + 1,
      messages: messages.length,
      lastMessage
    };
  };

  enrichMessageData = messages =>
    messages.map(msg => {
      const updatedMsg = Object.assign(msg);
      delete updatedMsg.marked_read; // "marked_read" property is not needed for now
      updatedMsg.likedByMe = false;
      updatedMsg.ownedByMe = false;
      updatedMsg.likes = 0;
      return updatedMsg;
    });
}

export default Chat;
